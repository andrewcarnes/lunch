Template.registerHelper('session', function(input){
	return Session.get(input);
});

Template.registerHelper('sessionArray', function(input, index){
	return Session.get(input)[index];
});

Template.registerHelper('getKidNumSessVar', function(){
	return Session.get("thisKidNumber");
});