AutoForm.debug(true);

AutoForm.addHooks(['insertApplicantForm'],{
    onSuccess: function() {
    	console.log(this.docId);
        Router.go('ContactInfo',{_id: this.docId});
    }
});

AutoForm.addHooks(['contactInfoForm'],{
    onSuccess: function() {
        console.log(this.docId);
        console.log(" contactInfoForm success");
        Router.go('Address',{_id: this.docId});
    }
});

AutoForm.addHooks(['addressForm'],{
    onSuccess: function() {
        console.log(this.docId);
        console.log(" addressForm success");
                console.log(this.docId);
        Router.go('Household',{_id: this.docId});
    }
});

AutoForm.addHooks(['householdCountForm'],{
    onSuccess: function() {
    	numK = numA = 1;
  
    	//Set session variable values to form field value s
    	numK = AutoForm.getFieldValue("numKids","householdCountForm");
    	Session.set("maxKid", numK);
    	numA = AutoForm.getFieldValue("numAdults","householdCountForm");
    	Session.set("maxAdult", numA);
        Router.go('HouseholdFlag',{_id: this.docId});
    }
});

AutoForm.addHooks(['householdFlagForm'],{
	before: function () {
				        hhFlag = false;

	},
    onSuccess: function() {
		        console.log(this.docId);
		        console.log(" householdFlagForm success");
		        hhFlag = false; 
		        hhFlag = AutoForm.getFieldValue("isHouseholdFlag","householdFlagForm");
		        if (hhFlag == false) {
			        Session.set("skipIncome", 0);
		        }
				if (hhFlag == true) {
			        Session.set("skipIncome", 1);
		        }
				var thisFlag = Session.get("skipIncome");
				console.log(thisFlag);
        Router.go('Kid',{_id: this.docId});
    }
});

AutoForm.addHooks(['kidForm'],{
    onSuccess: function() {

		        console.log(this.docId);
		        console.log(" kidForm success");
//		var currentKidNumber = Session.get("thisKid");
		//        currentKidNumber++;
//                console.log (currentKidNumber);
          //      Session.set("thisKidNumber", currentKidNumber);
		 Router.go('KidInfo',{_id: this.docId});
    }
});

AutoForm.addHooks(['kidInfoForm'],{
    onSuccess: function() {	
		        console.log(this.docId);
		        console.log(" kidInfoForm success");
                console.log("this K: "+Session.get("thisKid"));
         Router.go('KidIncome',{_id: this.docId});

}
});

AutoForm.addHooks(['kidIncomeForm'],{
    onSuccess: function() {	
		        console.log(this.docId);
		        console.log(" kidIncomeForm success");
                console.log("this K: "+Session.get("thisKid"));
        var currentKidNumber = Session.get("thisKid");
        currentKidNumber++;
        Session.set("thisKid", currentKidNumber);    
        		 console.log ("curr K: " +currentKidNumber);
        		             
		var maxNumKids = Session.get("maxKid");
		var isSkipIncome = Session.get("skipIncome");
		
        if (currentKidNumber < maxNumKids) {
        	Router.go('Kid',{_id: this.docId});
        	} else {
	            if (isSkipIncome == 1) {
		        	Router.go('HouseholdSSN',{_id: this.docId});
				} else {
					Router.go('Adult',{_id: this.docId});
				}}
}
});

AutoForm.addHooks(['adultForm'],{
    onSuccess: function() {
		        console.log(this.docId);
		        console.log(" AdultForm success");
//        var currentAdultNumber = Session.get("thisAdult");
//        currentKidNumber++;
//        console.log (currentAdultNumber);
  //      Session.set("thisKidNumber", currentKidNumber);
        Router.go('AdultIncomeA',{_id: this.docId});
    }
});

AutoForm.addHooks(['adultIncomeAForm'],{
    onSuccess: function() {
		        console.log(this.docId);
		        console.log(" AdultForm success");
//        var currentAdultNumber = Session.get("thisAdult");
//        currentKidNumber++;
//        console.log (currentAdultNumber);
  //      Session.set("thisKidNumber", currentKidNumber);
        Router.go('AdultIncomeB',{_id: this.docId});
    }
});

AutoForm.addHooks(['adultIncomeBForm'],{
    onSuccess: function() {
		        console.log(this.docId);
		        console.log(" AdultForm success");
//        var currentAdultNumber = Session.get("thisAdult");
//        currentKidNumber++;
//        console.log (currentAdultNumber);
  //      Session.set("thisKidNumber", currentKidNumber);
        Router.go('AdultIncomeC',{_id: this.docId});
    }
});

AutoForm.addHooks(['adultIncomeCForm'],{
    onSuccess: function() {
        console.log(this.docId);
        console.log(" AdultForm success");
		        var currentAdultNumber = Session.get("thisAdult");
		        currentAdultNumber++;
                console.log (currentAdultNumber);
                Session.set("thisAdult", currentAdultNumber);
        var maxNumAdults = Session.get("maxAdult");

        if (currentAdultNumber == maxNumAdults || currentAdultNumber > maxNumAdults) {
			Router.go('HouseholdSSN',{_id: this.docId});
		} else {
			 Router.go('Adult',{_id: this.docId});
		}
    }
});

AutoForm.addHooks(['householdSSNForm'],{
    onSuccess: function() {
        console.log(this.docId);
        console.log(" HouseholdSSNForm success");
        Router.go('HouseholdSignout',{_id: this.docId});
    }
});

AutoForm.addHooks(['householdSignoutForm'],{
    onSuccess: function() {
        console.log(this.docId);
        console.log(" HouseholdSignoutForm success");
        Router.go('Export',{_id: this.docId});
    }
});

/// AUTOFORM HOOKS: http://blogs.quovantis.com/meteor-generating-a-form-automatically-validating-it-and-saving-the-document/

//http://stackoverflow.com/questions/27903737/autoform-getfieldvalue-for-array-fields

// do math