Router.configure({
  controller: 'AppController',
  layoutTemplate: 'layoutMain'
});

Router.map(function() {

	this.route('Index', {
		path: '/'
	});	
	
	this.route('SignIn', {
		path: '/app/',
		controller: 'SignInController'
	});	

	this.route('ContactInfo/:_id', {
		name: 'ContactInfo',
		template: 'ContactInfo',
		controller: 'ContactInfoController'
	});

	this.route('Address/:_id', {
		name: 'Address',
		template: 'Address',
		controller: 'AddressController'		
	});
	
	this.route('Household/:_id', {
		name: 'Household',
		template: 'Household',
		controller: 'HouseholdController',
	});

	this.route('HouseholdFlag/:_id', {
		name: 'HouseholdFlag',
		template: 'HouseholdFlag',
		controller: 'HouseholdFlagController'	
	});
	
	this.route('Kid/:_id', {
		name: 'Kid',
		template: 'Kid'	,
		controller: 'KidController'
	});	

	this.route('KidInfo/:_id', {
		name: 'KidInfo',
		template: 'KidInfo'	,
		controller: 'KidInfoController'		
	});	

	this.route('KidIncome/:_id', {
		name: 'KidIncome',
		template: 'KidIncome'	,
		controller: 'KidIncomeController'		
	});	
		
	this.route('Adult/:_id', {
		name: 'Adult',
		template: 'Adult',
		controller: 'AdultController'
	});	

	this.route('AdultIncomeA/:_id', {
		name: 'AdultIncomeA',
		template: 'AdultIncomeA',
		controller: 'AdultIncomeAController'
	});	

	this.route('AdultIncomeB/:_id', {
		name: 'AdultIncomeB',
		template: 'AdultIncomeB',
		controller: 'AdultIncomeBController'
	});	
	
	this.route('AdultIncomeC/:_id', {
		name: 'AdultIncomeC',
		template: 'AdultIncomeC',
		controller: 'AdultIncomeCController'
	});	
			
	this.route('AdultInfo/:_id', {
		name: 'AdultInfo',
		template: 'AdultInfo',
		controller: 'AdultInfoController'
	});	
		
	this.route('HouseholdSSN/:_id', {
		name: 'HouseholdSSN',
		template: 'HouseholdSSN',
		controller: 'HouseholdSSNController'
	});	
	
	this.route('HouseholdSignout/:_id', {
		name: 'HouseholdSignout',
		template: 'HouseholdSignout',
		controller: 'HouseholdSignoutController'
	});			

	this.route('Export/:_id', {
		name: 'Export',
		template: 'Export',
		controller: 'ExportController'
	});	

});

//http://robertdickert.com/blog/2014/05/08/iron-router-first-steps/
// http://www.manuel-schoebel.com/blog/iron-router-tutorial

//http://meteortips.com/second-meteor-tutorial/iron-router-part-3/

//Router.route('/signin/:_id', function () {
//  var item = Applicants.findOne({_id: this.params._id});
//  this.render('applicant', {data: name1});
//}, {
//  name: 'applicant.show'
//});

//NEED A QUICK FIX? 
// It puts the data in the router.
// "Data context as a function" @http://www.manuel-schoebel.com/blog/iron-router-tutorial
//QUICK FIX OVER!

// GOOD DOCS @http://iron-meteor.github.io/iron-router/#hooks and @http://iron-meteor.github.io/iron-router/#path-and-link-template-helpers