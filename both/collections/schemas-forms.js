//SET FORM SCHEMAS


FormSchemas = {};

FormSchemas.ApplicantName = Schemas.Applicant.pick(['signindate','applicantName','applicantName.name1','applicantName.name2','applicantName.name3']);

FormSchemas.GetContactInfo = Schemas.Applicant.pick(['phone','email']);

FormSchemas.GetAddress = Schemas.Applicant.pick(['street','street2','city','state','zip']);

FormSchemas.GetHouseholdCount = Schemas.Applicant.pick(['numKids','numAdults']);

FormSchemas.GetHouseholdFlag = Schemas.Applicant.pick(['isHouseholdFlag', 'isHouseholdFlagCase']);

FormSchemas.GetKidInfo = Schemas.Applicant.pick(['kid', 'kid.$', 'kid.$.kidName', 'kid.$.kidName.name1','kid.$.kidName.name2','kid.$.kidName.name3', 'kid.$.kidFlagA',  'kid.$.kidFlagB',  'kid.$.kidFlagC']);

FormSchemas.GetKidName = Schemas.Applicant.pick(['kid', 'kid.$', 'kid.$.kidName', 'kid.$.kidName.name1','kid.$.kidName.name2','kid.$.kidName.name3']);

FormSchemas.GetKidFlags = Schemas.Applicant.pick(['kid', 'kid.$', 'kid.$.kidFlagA',  'kid.$.kidFlagB',  'kid.$.kidFlagC']);

FormSchemas.GetKidIncome = Schemas.Applicant.pick(['kid', 'kid.$', 'kid.$.kidIncome',  'kid.$.kidIncome.incomeType',  'kid.$.kidIncome.incomeAmount', 'kid.$.kidIncome.incomeFrequency']);


FormSchemas.GetAdultName = Schemas.Applicant.pick(['adult', 'adult.$', 'adult.$.adultName', 'adult.$.adultName.name1','adult.$.adultName.name2','adult.$.adultName.name3']);

FormSchemas.GetAdultInfo = Schemas.Applicant.pick(['adult', 'adult.$', 'adult.$.adultIncomeA', 'adult.$.adultIncomeA.incomeType', 'adult.$.adultIncomeA.incomeAmount',  'adult.$.adultIncomeA.incomeFrequency']);


FormSchemas.GetHouseholdSSN = Schemas.Applicant.pick(['householdSSN']);

FormSchemas.GetHouseholdSignout = Schemas.Applicant.pick(['signoutName', 'signouttime']);

///MainSchema.pick('stringArray', 'stringArray.$');
