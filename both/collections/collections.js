//SET COLLECTION SCHEMAS

Schemas = {};


Schemas.IncomeStreams = new SimpleSchema({
	incomeAmount: {
		type: Number,
		min: 0,
		max: 1000000,
		defaultValue: 0,
		optional: true
	},
	incomeFrequency: {
	type: Number,
	allowedValues: [52,26,24,12,1,0],
	defaultValue: 0,
	optional: true,
	autoform: {
		options: [
			{label: "Every Week", value: 52},
			{label: "Every 2 Weeks", value: 26},
			{label: "2 Times Every Month", value: 24},
			{label: "1 Time Every Month", value: 12},
			{label: "1 Time Every Year", value: 1},
			{label: "Never", value: 0}
		]
	}
}});

Schemas.Flags = new SimpleSchema({

  flagValue: {
    type: Boolean,
    defaultValue: false,
    optional: true
  },
  flagID: {
  	type: Number,
  	defaultValue: 0,
  	optional: true
  }
});

Schemas.PersonName = new SimpleSchema({
  name1: {
    type: String,
    label: "First",
    min: 0,
    max: 999
  },
  name2: {
    type: String,
    label: "Middle Initial",
    min: 0,
    max: 1
  },
  name3: {
    type: String,
    label: "Last Name",
    min: 0,
    max: 999
  }
});

Schemas.Applicant = new SimpleSchema({
  signindate: {
    type: Date,
    label: "Date Started",    
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }
  },
  applicantName: {
    type: Schemas.PersonName,
    label: "Your Name",
    optional: true
  },
  phone: {
    type: String,
    label: "Phone Number",
    optional: true
  },
  email: {
    type: String,
	regEx: SimpleSchema.RegEx.Email,  
    label: "Email",
    optional: true
  },
  street: {
    type: String,
    label: "Street Address",
    max: 200,
    optional: true
  },
  street2: {
    type: String,
    label: "Street Address 2 (if needed)",
    max: 200,
    optional: true
  },  
  city: {
    type: String,
    label: "City or County",
    max: 100,
    optional: true
  },
  state: {
type: String,
    label: "State",
    optional: true,
    allowedValues: ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"],
    autoform: {
      afFieldInput: {
        firstOption: "(Select a State)"
      }
    }
  },
  zip: {
    type: String,
    label: "Zip Code",
    optional: true,
	regEx: SimpleSchema.RegEx.ZipCode
  },
  numKids: {
    type: Number,
    label: "Number of Kids",
    defaultValue: 1,
    optional: true
  },
  numAdults: {
    type: Number,
    label: "Number of Adults",
    defaultValue: 1,
    optional: true
  },
  isHouseholdFlag: {
    type: Boolean,
    label: "Does anyone in your household receive assistance?",
    optional: true
  },
  isHouseholdFlagCase: {
    type: String,
    label: "If so, what is the case number?",
    optional: true
  },
  kid: {
	  type: Array,
	  optional: true,
	  minCount: 1,
	  maxCount: 100
  },  
  "kid.$": {
	  type: Object
  },
  "kid.$.kidName": {
    type: Schemas.PersonName,
    label: "Kid's Name",
    optional: true
  },
  "kid.$.kidFlagA": {
    type: Boolean,
    label: "Is a student",
    optional: true
  },
  "kid.$.kidFlagB": {
    type: Boolean,
    label: "Is in foster care",
    optional: true
  },
  "kid.$.kidFlagC": {
    type: Boolean,
    label: "Is a runaway, a migrant, or is homeless",
    optional: true
  },
  "kid.$.kidIncome": {
    type: Schemas.IncomeStreams,
    label: "Kid's Income",
    optional: true
  },
  "kid.$.verified": {
	  type: Boolean,
	  defaultValue: false,
	  optional: true
  },
  adult: {
	  type: Array,
	  optional: true,
	  minCount: 1,
	  maxCount: 100
  },  
  "adult.$": {
	  type: Object
  },  
  "adult.$.adultName": {
    type: Schemas.PersonName,
    label: "Adult's Name",
    optional: true
  },
  "adult.$.adultIncomeA": {
    type: Schemas.IncomeStreams,
    label: "Adult's Income from Work",
    optional: true
  },
  "adult.$.adultIncomeB": {
    type: Schemas.IncomeStreams,
    label: "Adult's Income from Assistance and Benefits",
    optional: true
  },
  "adult.$.adultIncomeC": {
    type: Schemas.IncomeStreams,
    label: "Adult's Income from Savings or Investments",
    optional: true
  },
  "adult.$.verified": {
	  type: Boolean,
	  defaultValue: true,
	  optional: true
  },
  isHouseholdVerified: {
	  type: Boolean,
	  defaultValue: false
  },
  householdSSN: {
	  type: Number,
	  label: "Last 4 digits of Social Security Number",
	  optional: true
  },
  signoutName: {
	  type: String,
	  label: "Sign Your Name",
	  optional: true
  },
  signouttime: {
    type: Date,
    label: "Date Completed", 
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()};
      } else {
        this.unset();  // Prevent user from supplying their own value
      }
    }	  
  }
});

Kids = new Mongo.Collection("kids");

Kids.attachSchema(Schemas.kid);

Applicants = new Mongo.Collection("applicant");

Applicants.attachSchema(Schemas.Applicant);

