//
//NOTE: "This is currently a bit tricky with Meteor since you can't precisely control file load order. You need to make sure parent RouteControllers are evaluated before child RouteControllers." - @http://iron-meteor.github.io/iron-router/#hooks

AppController = RouteController.extend({
	
	layoutTemplate: 'layoutMain',
	yieldTemplates: {
		'Header': {to: 'header'},
		'Footer': {to: 'footer'}
	},
	
  	data: function () {
		return Applicants.findOne({ _id: this.params._id });
		
	},
	
	action: function () {
		this.render();
	}
  
});

SignInController = AppController.extend({

	data: function() {
		templateData = {
				title: 'Your Name',
				text: 'Type your name (as completely as possible) into the boxes below.',
				_id: this.params._id 
			};
		return templateData;
	}
});



ContactInfoController = AppController.extend({

	data: function() {
		templateData = {
				title: 'Contact Information',
				text: 'How can we contact you? Type your phone number or email address.',
				_id: this.params._id 
			};
		return templateData;
	}
});


AddressController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Your Address',
				text: 'Type your address, as completely as possible. Click "Help" to see more information about this question.',
				_id: this.params._id 
			};
		return templateData;
	}
});

HouseholdController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Your Household',
				text: 'Your household is everyone who lives with you and shares income and expenses, even if he or she is not related (family).',
				text2: 'How many people live in your household?',
				_id: this.params._id 
			};
		return templateData;
	}
});

HouseholdFlagController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Household Assistance',
				text: 'Does anyone in your household (including you) currently participate in one or more of these assistance programs: SNAP, TANF, or FDPIR?',
				_id: this.params._id 
			};
		return templateData;
	}
});

KidController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Kids',
				text: 'Type the name(s) of all children in your household. Click the "+" button to add another child.',
				_id: this.params._id
				
			};
		return templateData;
		
		
	},
//	action: function () {
		// set the reactive state variable "appid" with the value of the docID from the URL
//		for (let i = 0; i< Session.get('numKids'); i++) {
//			this.render();
//		}
//		this.state.set('appId', this.params._id);
		
//	}
});

KidInfoController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Kids',
				text: 'Click to select the checkbox of any statements that are true about this child',
				_id: this.params._id,
//				thisKidFieldA: 'kid.0.kidFlagA'
//				kidIndex = 0;
//				kidIndexField = kid.0.isFlagA;
				
			};
		return templateData;
	},
//	action: function () {
		// set the reactive state variable "appid" with the value of the docID from the URL
//		for (let i = 0; i< Session.get('numKids'); i++) {
//			this.render();
//		}
//		this.state.set('appId', this.params._id);
		
//	}
});

KidIncomeController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Income',
				text: 'Sometimes children earn money from work, or receive money as gifts for holidays and special occassions. Does this child earn or receive money from anyone outside your household? If so, how much?',
				_id: this.params._id
				
			};
		return templateData;
	}
});

AdultController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Adult(s)',
				text: 'Type the name(s) of all adults in your household. Click "+" to add another adult.',
				_id: this.params._id
				
			};
		return templateData;
	}
});

AdultInfoController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Income',
				text: 'Please enter all sources of income for this adult. Click "+" to add another item or "-" to remove at item.',
				_id: this.params._id
				
			};
		return templateData;
	}
});

AdultIncomeAController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Income',
				text: 'How much money does this person earn from work?',
				_id: this.params._id
				
			};
		return templateData;
	}
});

AdultIncomeBController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Income',
				text: 'How much money does this person receive from public assistance, child support, or alimony?',
				_id: this.params._id
				
			};
		return templateData;
	}
});

AdultIncomeCController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Income',
				text: 'How much money does this person receive from savings, pensions, retirement, and other income?',
				_id: this.params._id
				
			};
		return templateData;
	}
});

HouseholdSSNController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Social Security Number',
				text: 'Please enter the last four (4) digits of the social security number for the primary wage earner or any other adult in your household.',
				text2: 'Or, if there is no social security number for you or any other adult in your household, click "No Social Security" to select that option.',
				_id: this.params._id
				
			};
		return templateData;
	}
});


HouseholdSignoutController = AppController.extend({
	data: function() {
		templateData = {
				title: 'Sign Here',
				text: 'When you sign the form, you make a promise that all your answers to the questions were true. To sign the form, click to select "I promise" and type your name.',
				legalText: 'The person signing is furnishing true information and to advise that person that the application is being made in connection with the receipt of Federal funds;',
				legalText2: 'School officials may verify the information on the application; and',
				legalText3: 'Deliberate misrepresentation of the information may subject the applicant to prosecution under State and Federal statutes.',
				_id: this.params._id
				
			};
		return templateData;
	}
});


ExportController = AppController.extend({
//STUFF HERE
});